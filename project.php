<?php

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

session_start();

require_once 'vendor/autoload.php';

DB::$dbName = 'webportdb';
DB::$user = 'userMLY';
DB::$encoding = 'utf8';
DB::$password = 'EK5HGlohIwarY2Ts'; 


// ======================================= ERROR handlers
 DB::$error_handler = 'sql_error_handler';
 DB::$nonsql_error_handler = 'nonsql_error_handler';


 // Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(   
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));


$twig = $app->view()->getEnvironment();
$twig->addGlobal('userSession', $_SESSION['user']);

if (!isset($_SESSION['user'])) {
    $_SESSION['user'] = array();
}

// ============================================================= INDEX ================================================================
$app->get('/', function() use ($app) {
  
    
   $app->render('/index.html.twig');
});
